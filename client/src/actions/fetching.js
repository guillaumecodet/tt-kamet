import {FETCHING} from '../constants/database';

export default function fetching() {
        return {
            type: FETCHING
        }
    }