import {LOAD_DATA, CURRENT_FIELD} from '../constants/database';
import fetching from './fetching';
import errorFetched from './errorFetched';
import {fetchApi} from '../utils';

export default {
    loadData: (field) => {
        return dispatch => {
            dispatch(newField(field));
            dispatch(fetching());
            return fetchApi(`items/${field}`)
                .then((res) => {
                    return dispatch(loadData(res))
                })
        }
    }
}

function newField(field) {
    return {type:CURRENT_FIELD, field}
}

function loadData(data) {
    return {
        type: LOAD_DATA,
        data
    }
}