import {ERROR_FETCHED} from '../constants/database';

export default function errorFetched() {
        return {
            type: ERROR_FETCHED
        }
    }