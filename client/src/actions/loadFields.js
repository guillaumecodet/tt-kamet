import {LOAD_FIELDS} from '../constants/database';
import fetching from './fetching';
import errorFetched from './errorFetched';
import {fetchApi} from '../utils';

export default {
    loadFields: () => {
        return dispatch => {
            dispatch(fetching());
            return fetchApi('fields')
                .then((res) => {
                    return dispatch(loadFields(res))
                })
                .catch(() => {
                    return dispatch(errorFetched());
                })
        }
    }
}

function loadFields(fields) {
    return {
        type: LOAD_FIELDS,
        fields
    }
}