import { combineReducers } from 'redux'
import { routeReducer } from 'redux-simple-router'
import database from './database'

export default combineReducers({
  database,
  routing: routeReducer
})
