import { LOAD_FIELDS, LOAD_DATA, CURRENT_FIELD, FETCHING, ERROR_FETCHED } from 'constants/database'

const initialState = { fields: []}
export default function databaseReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_FIELDS:
      return Object.assign({}, state, {
        fields: action.fields,
        fetching: false
      });
    case LOAD_DATA:
      return Object.assign({}, state, {
        data: action.data,
        fetching: false
      });
    case CURRENT_FIELD:
      return Object.assign({}, state, {
        field: action.field
      });
    case FETCHING:
      return Object.assign({}, state, {
        error: false,
        fetching: true
      });
    case ERROR_FETCHED:
      return Object.assign({}, state, {
        error: true
      });
    default:
      return state;
  }
}