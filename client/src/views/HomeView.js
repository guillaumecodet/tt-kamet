import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import loadData from 'actions/loadData';
import loadFields from 'actions/loadFields';
import FieldsList from 'components/FieldsList';
import ItemsList from 'components/ItemsList';

const mapStateToProps = (state) => {
    return ({
        counter: state.counter,
        database: state.database,
        routerState: state.router
    })
}

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators({...loadFields, ...loadData}, dispatch)
});

export class HomeView extends React.Component {

    constructor(props) {
        super(props);
        this.loadData = this.loadData.bind(this);
    }

    componentWillMount() {
        this.props.actions.loadFields();
    }

    loadData(field) {
        this.props.actions.loadData(field);
    }

    render() {
        return (
            <div className="container text-center">
                {this.props.database.error && <div className="alert alert-danger">
                    An error occurred while retrieving the resource
                </div>}

                    <FieldsList fields={this.props.database.fields} onClick={(field) => this.loadData(field)}
                                currentField={this.props.database.field}/>
                { this.props.database.fetching && <div className="alert alert-info">
                    Loading
                </div>}
                { !this.props.database.fetching &&
                    <ItemsList data={this.props.database.data} currentField={this.props.database.field}/>
                }
            </div>
        )
    }
}

HomeView.propTypes = {
    actions: React.PropTypes.object,
    counter: React.PropTypes.number
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeView);
