import {createConstants} from '../utils'

export default createConstants(
    'LOAD_FIELDS',
    'LOAD_DATA',
    'CURRENT_FIELD',
    'FETCHING',
    'ERROR_FETCHED'
)
