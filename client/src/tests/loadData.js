import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import chai from 'chai';
const expect = chai.expect;

import loadData from '../actions/loadData'

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Load data', () => {
    it('should call all of the actions', () => {
        const store = mockStore({})
        const expectedActions = [ 'CURRENT_FIELD', 'FETCHING', 'LOAD_DATA' ];

        fetchMock.get('*', ['field1', 'field2'])
        store.dispatch(loadData.loadData())
            .then(() => {
                const actualActions = store.getActions().map(action => action.type);
                expect(actualActions).to.deep.equal(expectedActions);
            });

        fetchMock.restore()
    });
});