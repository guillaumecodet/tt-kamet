import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import chai from 'chai';
const expect = chai.expect;

import loadFields from '../actions/loadFields';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Load field', () => {
    it('should call all of the actions', () => {
        const store = mockStore({})
        const expectedActions = [ 'FETCHING', 'LOAD_FIELDS' ];

        fetchMock.get('*', ['field1', 'field2'])
        store.dispatch(loadFields.loadFields())
            .then(() => {
                const actualActions = store.getActions().map(action => action.type);
                expect(actualActions).to.deep.equal(expectedActions);
            });

        fetchMock.restore();
    });
});