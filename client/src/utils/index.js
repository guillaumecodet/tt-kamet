const API_URL = 'http://localhost:8080/api/';
import 'whatwg-fetch';

export function createConstants(...constants) {
    return constants.reduce((acc, constant) => {
        acc[constant] = constant
        return acc
    }, {})
}

export function fetchApi(path) {
    return fetch(`${API_URL}${path}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
        }
    }).then((response) => response.json())
}