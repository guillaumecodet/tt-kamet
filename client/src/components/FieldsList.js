import React from 'react';

const FieldsList = (props) => {
       return  (<ul className="list-inline ">
            {props.fields && props.fields.map((field, key) => {
                return <li key={key}>
                    <button
                        onClick={() => props.onClick(field)}
                        className={"btn text-capitalize " + ((field === props.currentField) ? 'btn-success' :  'btn-default')} type="submit">{field}</button></li>;
            })}
        </ul>)
};

export default FieldsList;