import React from 'react';

const ItemsList = (props) => (<div> {props.data &&
        <table className="table">
            <thead>
            <tr>
                <th className="text-capitalize text-center">{props.currentField}</th>
                <th className="text-center">Age average</th>
                <th className="text-center">Count</th>
            </tr>
            </thead>
            <tbody>
            {props.data.map((item) => {
                return <tr>
                    <td>{item.requestedField}</td>
                    <td>{item.ageAverage}</td>
                    <td>{item.count}</td>
                </tr>
            })}
            </tbody>
        </table>}</div>
    )
;

export default ItemsList;