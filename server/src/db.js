import sqlite3 from 'sqlite3';
import {registry} from './lib/util';

export default () => {
    return new Promise((resolve, reject) => {
		const db = new sqlite3.Database('./db/us-census.db', (e) => {
            if (e) {
                return reject(e);
            }
            registry('db', db);
            resolve();
        });
    });
}
