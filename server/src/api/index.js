import { Router } from 'express';

import {getItemsBy, getFields} from '../services/db';


export default () => {
	let api = Router();

	api.get('/items/:field', (req, res) => {
		getItemsBy(req.params.field)
			.then((response) => {
				res.json(response);
			})
			.catch((e) => {
				res.status(404).send();
			});

	});

	api.get('/fields', (req, res) => {
		getFields()
			.then((response) => {
				res.json(response);
			})
			.catch((e) => {
				res.status(500).send();
			});

	});

	return api;
}
