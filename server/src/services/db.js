import {registry} from '../lib/util';

const additionalField = [
    'round(avg(age), 2) as ageAverage',
    'count(*) as count'
];
const tableName = 'census_learn_sql';

let fieldsList = [];

export function getItemsBy(field, offset = 0, limit = 100) {
    field = decodeURI(field);
    return new Promise((resolve, reject) => {
        getFields()
            .then((fields) => {
                if (fields.indexOf(field) === -1) {
                    return reject('Unknow column');
                }
                registry('db').all(
                    `SELECT ${additionalField.join(', ')}, "${field}" as requestedField 
                    FROM ${tableName} 
                    GROUP BY "${field}" 
                    ORDER BY count DESC 
                    LIMIT ${offset}, ${limit}`, (err, rows) => {
                    resolve(rows);
                });
            });
    })
}

export function getFields() {
    return new Promise((resolve, reject) => {
        if (fieldsList.length) {
            return resolve(fieldsList);
        }
        registry('db').all(`PRAGMA table_info([${tableName}]);`, (err, rows) => {
            if (err) {
                return reject(`Can't load the fields`);
            }
            fieldsList = rows.map((row) => row.name).sort();
            resolve(fieldsList);
        });
    })
}
