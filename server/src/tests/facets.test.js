const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index').default;
const should = chai.should();

chai.use(chaiHttp);

describe('Api', () => {
    describe('/GET inexisting api endpoint', () => {
        it('it should return a 404', (done) => {
            chai.request(server)
                .get('/api/this-route-does-not-exist')
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
                });
        });
    });

    describe('/GET fields', () => {
        it('it should GET all the fields', (done) => {
            chai.request(server)
                .get('/api/fields')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.gte(0);
                    done();
                });
        });
    });

    describe('/GET items', () => {
        let fields;
        beforeEach((done) => {
            chai.request(server)
                .get('/api/fields')
                .end((err, res) => {
                    fields = res.body;
                    done();
                });
        });

        it('it should GET some items', (done) => {
            chai.request(server)
                .get(`/api/items/${fields[0]}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.gte(0);
                    done();
                });
        });

        it('it should return a 404 if no field', (done) => {
            chai.request(server)
                .get(`/api/items`)
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
                });
        });

        it('it should return a 404 if a fake field', (done) => {
            chai.request(server)
                .get(`/api/items/fake-field`)
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
                });
        });
    });

});