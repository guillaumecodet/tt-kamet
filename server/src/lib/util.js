const registryObject = {};
export function registry(key, value) {
	if (value) {
		registryObject[key] = value;
	} else {
		return registryObject[key];
	}
}
