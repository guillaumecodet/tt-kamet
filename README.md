### Installation
```
npm run setup
```
### Usage
```
npm run start:client
```
```
npm run start:server
```
### Tests
```
npm run test:client
```
```
npm run test:server
```